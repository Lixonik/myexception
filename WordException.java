public class WordException extends RuntimeException {
    public WordException() {
        super();
    }

    public WordException(String message) {
        super(message);
    }
}