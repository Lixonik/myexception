/*
    Input:
    Здравствуйте, меня зовут Борис. Я собираюсь изучить, как работают exc44444eptions в языке программирования 666Java666.
    Output:
    Слово "Здравствуйте" состоит из более 10 букв
    Слово "программирования" состоит из более 10 букв
 */

import java.util.Scanner;

public class MyException {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        MyException attempt = new MyException();
        attempt.parseText(text);
    }

    public void parseText(String text) throws WordException {
        for (String word : text.split("[^a-zA-Zа-яА-Я0-9_\\-]")) {
            try {
                if (word.replaceAll("[^a-zA-Zа-яА-Я]", "").length() > 10) {
                    throw new WordException("Слово \""+ word + "\" состоит из более 10 букв");
                }
            } catch (WordException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
